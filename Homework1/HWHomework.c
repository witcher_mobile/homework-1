//
//  HWHomework.c
//  Homework1
//
//  Created by Vladislav Grigoriev on 21/09/16.
//  Copyright © 2016 com.inostudio.ios.courses. All rights reserved.
//

#include "HWHomework.h"
#include "stdlib.h"
#include <math.h>
#include <time.h>
#include <ctype.h>
#include <string.h>

/*
 * Задание 1. (1 балл)
 *
 * Необходмо реализовать метод, который создаст матрицу и заполнит её случайными числами от -100 до 100.
 *
 * Параметры:
 *      rows - количество строк в матрице;
 *      columns - количество столбцов в матрице;
 *
 * Возвращаемое значение:
 *      Матрица размером [rows, columns];
 */
long** createMatrix(const long rows, const long columns) {
    srand(time(NULL));                  // Инициализация ГСЧ
    
    const long minValue     = -100;     // Минимальное значение
    const long widthValue   = 201;      // Ширина выборки
    
    // Выделяем память под строки
    long **matrix = (long**)malloc(sizeof(long*) * rows);
    
    for(int i = 0; i < rows; ++i){
        // Выделяем память под столбцы
        matrix[i] = (long*)malloc(sizeof(long) * columns);
        
        // Генерация СЧ для текущей строки
        for(int j = 0; j < columns; ++j){
            matrix[i][j] = minValue + rand() % widthValue;
        }
    }
    
    return matrix;
}


/*
 * Задание 2.
 *
 * Необходимо реализовать метод, котрый преобразует входную строку, состоящую из чисел, в строку состоящую из числительных.
 * Задание имеет два уровня сложности: обычный и высокий.
 *
 * Параметры:
 *      numberString - входная строка (например: "1", "123.456");
 *
 * Возвращаемое значение:
 *      Обычный уровень сложности (1 балл):
 *             Строка, содержащая в себе одно числительное или сообщение о невозможности перевода;
 *             Например: "один", "Невозможно преобразовать";
 *
 *      Высокий уровень сложности (2 балла):
 *              Строка, содержащая себе числительные для всех чисел или точку;
 *              Например: "один", "один два три точка четыре пять шесть";
 *
 *
 */
// Функция для возврата текста числительного
char* stringNumber(char number){
    switch(number){
        case '0': return "ноль \0";
        case '1': return "один \0";
        case '2': return "два \0";
        case '3': return "три \0";
        case '4': return "четыре \0";
        case '5': return "пять \0";
        case '6': return "шесть \0";
        case '7': return "семь \0";
        case '8': return "восемь \0";
        case '9': return "девять \0";
        case '.': return "точка \0";
        default: return "!none!\0";
            
    };
    return NULL;
}

// Валидные значения
// 0.36, .36, 36.
char* stringForNumberString(const char *numberString) {
    char *errorMessage = "Невозможно преобразовать\0";
    
    // Проверка на пустоту строки
    if(strlen(numberString) <= 0) return errorMessage;
    
    // Динамически выделяем память для результата
    // (выделяем в два раза больше так как юзаем кирилицу)
    const unsigned int max_size = 100;
    char *resultString = (char*) malloc(sizeof(char) * 2 * (max_size + 1));
    
    // Инициализируем указатели на строки
    char *resource      = numberString;
    char *destinition   = resultString;
    
    // Флаг числа
    int isDigit = 0;
    
    // Пока не дошли до конца исходной строки
    // (может быть преждевременный выход из цикла)
    while(*resource != '\0'){
        
        // Закрываем строку если преждевременно выйдем
        *destinition = '\0';
        
        // Определяем число - не число
        isDigit = isdigit(*resource);
        
        // Если не число и не точка то умываем руки
        if(!isDigit && *resource != '.')
            return errorMessage;
        
        // Получаем числительное
        char *temp = stringNumber(*resource);
        
        // Определяем длину текста
        unsigned long len = strlen(temp);
        
        // Проверка выхода за границы массива
        // (юзаем адресную арифметику)
        if(((destinition + len) - resultString) >= 2 * max_size) break;
        
        // Делаем конкатенацию строк
        strcat(destinition, temp);
        
        // Наращиваем указатели
        destinition += len;
        resource++;
    }
    
    return resultString;
}



/*
 * Задание 3.
 *
 * Необходимо реализовать метод, который возвращает координаты точки в зависимости от времени с начала анимации.
 * Задание имеет два уровня сложности: обычный и высокий.
 *
 * Параметры:
 *      time - время с начала анимации в секундах (например: 0 или 1.234);
 *      canvasSize - длина стороны квадрата, на котором происходит рисование (например: 386);
 *
 * Возвращаемое значение:
 *      Структура HWPoint, содержащая x и y координаты точки;
 *
 * Особенность:
 *      Начало координат находится в левом верхнем углу.
 *
 * Обычный уровень сложности (2 балла):
 *      Анимация, которая должна получиться представлена в файле Default_mode.mov.
 *
 * Высокий уровень сложности (3 балла):
 *      Анимация, которая должна получиться представлена в файле Hard_mode.mov.
 *
 * PS: Не обязательно, чтобы ваша анимация один в один совпадала с примером.
 * PPS: Можно реализовать свою анимацию, уровень сложности которой больше или равен анимациям в задании.
 */

HWPoint pointForAnimationTime(const double time, const double canvasSize) {
    const double PI = 3.1415926535;         // Значение числа Пи
    const double a  = 45;                   // Шаг спирали
    
    static double phi = 0.0;                // Значение угла движения
    
    // Рассчитываем координаты спирали (спираль Архимеда)
    double x = (canvasSize / 2) + ((a / (2 * PI))  * phi * cos(phi));
    double y = (canvasSize / 2) + ((a / (2 * PI))  * phi * sin(phi));
    
    // Если вышли за границу отрисовки, то идем в обратном направлении
    if((x < 0.0 || x >= canvasSize) || (y < 0.0 || y >= canvasSize)){
        phi = -phi;
    }
    
    // Наращиваем значение угла
    phi += 0.1;
    
    return (HWPoint){x,  y};
}

